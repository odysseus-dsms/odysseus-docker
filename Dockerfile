FROM openjdk:8-jre

EXPOSE 8888

RUN mkdir -p /root/.odysseus/autostart
VOLUME /root/.odysseus

COPY odysseus-server.zip /odysseus-server.zip
RUN mkdir /usr/lib/odysseus && \
	unzip /odysseus-server.zip -d /usr/lib/odysseus && \
	chmod +x /usr/lib/odysseus/odysseus && \
	rm /odysseus-server.zip

ENTRYPOINT ["/usr/lib/odysseus/odysseus"]
